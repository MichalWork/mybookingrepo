module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "myBooking",
  },
  plugins: ["gatsby-plugin-gatsby-cloud"],
};
